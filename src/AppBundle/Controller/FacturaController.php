<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Factura;
use AppBundle\Form\FacturaType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class FacturaController extends Controller
{
    public function addAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        if(empty($em->getRepository('AppBundle:Cliente')->findBy(array(), null, 1)))
            return $this->redirectToRoute('app');

        $factura = new Factura();
        $form = $this->createForm(FacturaType::class, $factura);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($factura);
            $em->flush();
            return $this->redirectToRoute('item_add', array('id_factura' => $factura->getId()));
        }

        return $this->render('default/form.html.twig', [
            "title" => 'Factura - Nuevo',
            "form" => $form->createView()
        ]);
    }

    public function updateAction($id, Request $request)
    {
        $factura = $this->getDoctrine()->getManager()->getRepository('AppBundle:Factura')->find($id);
        $form = $this->createForm(FacturaType::class, $factura);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($factura);
            $em->flush();
            return $this->redirectToRoute('app');
        }

        $title = 'Factura Nro '.$factura->getId().' <a href="'.$this->generateUrl('factura_list', array('id'=>$factura->getId())).'">Ver Items</a>';

        return $this->render('default/form.html.twig', [
            "title" => $title,
            "form" => $form->createView()
        ]);
    }

    /**
     * Borrar todos los items
     * @param $id
     * @return Response
     */
    public function removeAction($id)
    {
        $manager = $this->getDoctrine()->getManager();
        $factura = $manager->getRepository('AppBundle:Factura')->find($id);

        if(!isset($factura))
            return new Response('', 404);

        $manager->remove($factura);
        $manager->flush();

        return $this->redirectToRoute('app');
    }

    public function listAction($id='')
    {
        $em = $this->getDoctrine()->getManager();
        $factura = $em->getRepository('AppBundle:Factura')->find($id);

        if(is_null($factura))
            return $this->redirectToRoute('app');

        $items = $em->getRepository('AppBundle:Item')->findBy(array('factura'=>$id));

        return $this->render('default/facturas_list.html.twig', array(
            'factura' => $factura,
            'items' => $items
        ));
    }
}
