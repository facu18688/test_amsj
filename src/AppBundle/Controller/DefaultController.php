<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Cliente;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{

    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $clientes = $em->getRepository('AppBundle:Cliente')->findBy(array(), array('id'=>'desc'), 10);
        $facturas = $em->getRepository('AppBundle:Factura')->findBy(array(), array('id'=>'desc'), 10);

        return $this->render('default/index.html.twig', array(
            'clientes' => $clientes,
            'facturas' => $facturas
        ));
    }
}
