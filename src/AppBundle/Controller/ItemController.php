<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Item;
use AppBundle\Form\ItemType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ItemController extends Controller
{
    public function addAction($id_factura='', Request $request)
    {
        if(empty($id_factura))
            return $this->redirectToRoute('factura_add');

        $item = new Item();

        $form = $this->createForm(ItemType::class, $item);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $facturaEntity = $em->getRepository('AppBundle:Factura')->find($id_factura);

            if(is_null($facturaEntity))
                return $this->redirectToRoute('factura_add');

            $item->setFactura($facturaEntity);
            $em->persist($item);
            $em->flush();
            return $this->redirectToRoute('item_add', array('id_factura' => $facturaEntity->getId()));
        }

        return $this->render('default/form.html.twig', [
            "title" => 'Detalle Factura <a href="'.$this->generateUrl('factura_list', array('id'=>$id_factura)).'">['.$id_factura.']</a> / Item - Nuevo',
            "form" => $form->createView()
        ]);
    }

    public function updateAction($id, Request $request)
    {
        $item = $this->getDoctrine()->getManager()->getRepository('AppBundle:Item')->find($id);
        $form = $this->createForm(ItemType::class, $item);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($item);
            $em->flush();
            return $this->redirectToRoute('app');
        }

        $title = 'Item '.$item->getProducto().' ('.$item->getId().')';

        return $this->render('default/form.html.twig', [
            "title" => $title,
            "form" => $form->createView()
        ]);
    }

    /**
     * @param $id
     * @return Response
     */
    public function removeAction($id)
    {
        $manager = $this->getDoctrine()->getManager();
        $item = $manager->getRepository('AppBundle:Item')->find($id);

        $factura_id = $item->getFactura();

        if(!isset($item))
            return new Response('', 404);

        $manager->remove($item);
        $manager->flush();

        return $this->redirectToRoute('factura_list', array('id'=>$factura_id));
    }
}
