<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Cliente;
use AppBundle\Form\ClienteType;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ClienteController extends Controller
{
    public function addAction(Request $request)
    {
        $cliente = new Cliente();
        $form = $this->createForm(ClienteType::class, $cliente);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($cliente);
            $em->flush();
            return $this->redirectToRoute('app');
        }

        return $this->render('default/form.html.twig', [
            "title" => 'Cliente - Nuevo',
            "form" => $form->createView()
        ]);
    }

    public function updateAction($id, Request $request)
    {
        $cliente = $this->getDoctrine()->getManager()->getRepository('AppBundle:Cliente')->find($id);

        if(is_null($cliente))
            return $this->redirectToRoute('app');

        $form = $this->createForm(ClienteType::class, $cliente);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($cliente);
            $em->flush();
            return $this->redirectToRoute('app');
        }

        $title = 'Cliente - '.$cliente->getNombre().' '.$cliente->getApellido().' ('.$cliente->getId().')';

        return $this->render('default/form.html.twig', [
            "title" => $title,
            "form" => $form->createView()
        ]);
    }

    public function removeAction($id)
    {
        $manager = $this->getDoctrine()->getManager();
        $cliente = $manager->getRepository('AppBundle:Cliente')->find($id);

        if(!isset($cliente))
            return new Response('', 404);

        $manager->remove($cliente);
        $manager->flush();

        return $this->redirectToRoute('app');
    }

}
