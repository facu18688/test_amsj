<?php

namespace AppBundle\Entity;

/**
 * Item
 */
class Item
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $producto = '';

    /**
     * @var integer
     */
    private $cantidad;

    /**
     * @var string
     */
    private $costoUnitario;

    /**
     * @var string
     */
    private $impuestos;

    /**
     * @var string
     */
    private $costoTotal;

    /**
     * @var \AppBundle\Entity\Factura
     */
    private $factura;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set producto
     *
     * @param string $producto
     *
     * @return Item
     */
    public function setProducto($producto)
    {
        $this->producto = $producto;

        return $this;
    }

    /**
     * Get producto
     *
     * @return string
     */
    public function getProducto()
    {
        return $this->producto;
    }

    /**
     * Set cantidad
     *
     * @param integer $cantidad
     *
     * @return Item
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    /**
     * Get cantidad
     *
     * @return integer
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * Set costoUnitario
     *
     * @param string $costoUnitario
     *
     * @return Item
     */
    public function setCostoUnitario($costoUnitario)
    {
        $this->costoUnitario = $costoUnitario;

        return $this;
    }

    /**
     * Get costoUnitario
     *
     * @return string
     */
    public function getCostoUnitario()
    {
        return $this->costoUnitario;
    }

    /**
     * Set impuestos
     *
     * @param string $impuestos
     *
     * @return Item
     */
    public function setImpuestos($impuestos)
    {
        $this->impuestos = $impuestos;

        return $this;
    }

    /**
     * Get impuestos
     *
     * @return string
     */
    public function getImpuestos()
    {
        return $this->impuestos;
    }

    /**
     * Set costoTotal
     *
     * @param string $costoTotal
     *
     * @return Item
     */
    public function setCostoTotal($costoTotal)
    {
        $this->costoTotal = $costoTotal;

        return $this;
    }

    /**
     * Get costoTotal
     *
     * @return string
     */
    public function getCostoTotal()
    {
        return $this->costoTotal;
    }

    /**
     * Set factura
     *
     * @param \AppBundle\Entity\Factura $factura
     *
     * @return Item
     */
    public function setFactura(\AppBundle\Entity\Factura $factura = null)
    {
        $this->factura = $factura;

        return $this;
    }

    /**
     * Get factura
     *
     * @return \AppBundle\Entity\Factura
     */
    public function getFactura()
    {
        return $this->factura;
    }
}

